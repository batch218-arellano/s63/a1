import {useState, useEffect} from 'react';
import { Card, Button } from 'react-bootstrap';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';


export default function CourseCard({course}) {


    

	const {name, description, price, _id} = course;
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			
		})
	}, [])

	function handleDelete() {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${course._id}`, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({isActive: false})
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);
        setCourses(courses.filter(c => c._id !== course._id));
    });
  }

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
	         <Button className="bg-danger" onClick={handleDelete}>Remove</Button>
	    </Card.Body>
	</Card>
	)
}

CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}
