import {useState, useEffect} from 'react';

export default function ClientCard {

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>PhP {price}</Card.Text>
	        <Button className="bg-primary" as={Link} to={`/courses/${_id}`} >Details</Button>
	         <Button className="bg-danger" onClick={handleDelete}>Remove</Button>
	    </Card.Body>
	</Card>
	)
}
